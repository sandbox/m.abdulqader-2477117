<?php

/**
 * Pileup Panes general settings.
 */
function pileup_settings_form($form, &$form_state) {

  $form = array();

  $settings = variable_get('pileup_settings', array());

  $form['pileup_breakpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Break point'),
    '#description' => t('Number <b>px</b> where your panes piled up and apply owl carousel'),
    '#required' => TRUE,
    '#default_value' => (isset($settings['pileup_breakpoint']) ? $settings['pileup_breakpoint'] : ''),
  );

  $form['pileup_number_of_groups'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of groups'),
    '#description' => t('When you save this you will be able to edit your groups label and settings'),
    '#required' => TRUE,
    '#default_value' => (isset($settings['pileup_number_of_groups']) ? $settings['pileup_number_of_groups'] : ''),
  );

  $number_of_groups = (isset($settings['pileup_number_of_groups']) ? $settings['pileup_number_of_groups'] : 0);
  if ($number_of_groups) {

    // Generate a settings form for each group.
    $form['groups_settings'] = array(
      '#type' => 'vertical_tabs',
    );
    for ($group = 1; $group <= $number_of_groups; $group++) {

      $form['pileup_group_' . $group . '_wrapper'] = array(
        '#type' => 'fieldset',
        '#title' => t("Group @num Settings", array('@num' => $group)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#group' => 'groups_settings'
      );
      $form['pileup_group_' . $group . '_wrapper']['pileup_group_' . $group . '_label'] = array(
        '#type' => 'textfield',
        '#title' => t("Group @num Label", array('@num' => $group)),
        '#default_value' => (isset($settings['pileup_group_' . $group . '_label']) ? $settings['pileup_group_' . $group . '_label'] : ''),
      );
      $form['pileup_group_' . $group . '_wrapper']['pileup_group_' . $group . '_settings'] = array(
        '#type' => 'select',
        '#title' => t("Group @num Owl Carousel preset", array('@num' => $group)),
        '#options' => owlcarousel_instance_callback_list(),
        '#default_value' => (isset($settings['pileup_group_' . $group . '_settings']) ? $settings['pileup_group_' . $group . '_settings'] : ''),
      );
    }
  }

  $form = system_settings_form($form);

  // We don't want to call system_settings_form_submit(), so change #submit.
  array_pop($form['#submit']);
  $form['#submit'][] = 'pileup_settings_form_submit';

  return $form;
}

/*
 * FAPI to save all settings in one variable.
 */

function pileup_settings_form_submit($form, &$form_state) {

  // Exclude unnecessary elements before saving.
  form_state_values_clean($form_state);

  $values = $form_state['values'];

  variable_set('pileup_settings', $values);
  drupal_set_message(t('The configuration options have been saved.'));

  cache_clear_all();
}
