<?php

/**
 * Implements hook_menu().
 */
function pileup_panes_menu() {
  $items['admin/config/user-interface/pileup'] = array(
    'title' => 'Pileup panes',
    'description' => 'Pileup settings list.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pileup_settings_form'),
    'access arguments' => array('administer pileup settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'pileup_panes.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function pileup_panes_permission() {
  return array(
    'administer pileup settings' => array(
      'title' => t('Administer pileup panes settings'),
    ),
  );
}

/**
 * Implementation of hook_panels_ipe_pane_links_alter().
 */
function pileup_panes_panels_ipe_pane_links_alter(&$links, $context) {
  $pane = $context['pane'];
  $renderer = $context['renderer'];

  if (user_access('administer advanced pane settings')) {
    $new_link['css'] = array(
      'title' => t('CSS'),
      'href' => $renderer->get_url('pane-css', $pane->pid),
      'html' => TRUE,
      'attributes' => array(
        'class' => array('ctools-use-modal', 'pane-css'),
        'title' => t('CSS'),
      ),
    );
    array_splice($links, array_search('style', array_keys($links)) + 1, 0, $new_link);
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function pileup_panes_form_panels_edit_configure_pane_css_form_alter(&$form, &$form_state) {
  $pane = &$form_state['pane'];

  // Set weight for save to be in bottom
  $form['next']['#weight'] = 100;

  $form['pileup'] = array(
    '#type' => 'select',
    '#title' => t('Pile me up in'),
    '#options' => _pileup_groups_list(),
    '#default_value' => isset($pane->css['pileup']) ? $pane->css['pileup'] : '',
    '#description' => t('Combine panes in defined break points'),
  );

  $form['#submit'][] = 'pileup_panes_configure_pane_css_form_submit';
}

/**
 * FAPI submission function for the CSS configure form.
 */
function pileup_panes_configure_pane_css_form_submit($form, &$form_state) {
  $pane = &$form_state['pane'];
  $pane->css['pileup'] = $form_state['values']['pileup'];
}

/*
 * Implementation of template_preprocess_panels_pane.
 */
function pileup_panes_preprocess_panels_pane(&$vars) {
  $pane = &$vars['pane'];

  // Add module class to pane
  if (isset($pane->css['pileup']) && $pane->css['pileup']) {
    $vars['classes_array'][] = $pane->css['pileup'];
    $vars['classes_array'][] = 'pile-me-up';

    $settings = variable_get('pileup_settings', array());
    $instance = $settings[str_replace('-', '_', $pane->css['pileup']) . '_settings'];

    // Load carousel settings from pileup group.
    $instance_settings = _owlcarousel_return_carousel_instance_settings($instance);

    $vars['content']['#attached'] = array(
      'js' => array(
        array(
          'data' => drupal_get_path('module', 'pileup_panes') . '/includes/js/pileup.panes.js',
          'type' => 'file',
          'scope' => 'footer'
        ),
        array(
          'data' => array('pileup' => array(
              'breakpoint' => $settings['pileup_breakpoint'],
              $pane->css['pileup'] => $instance_settings,
            ),),
          'type' => 'setting'
        )
      ),
      'css' => array(
        array(
          'data' => drupal_get_path('module', 'pileup_panes') . '/includes/css/pileup-panes.css',
          'type' => 'file',
          'scope' => 'footer'
        ),
      ),
      'library' => array(
        array(
          'owlcarousel',
          'owl-carousel'
        )
      ),
    );
  }
}

/*
 * Get groups list with their labels.
 */
function _pileup_groups_list() {

  $settings = variable_get('pileup_settings', array());
  $groups = (isset($settings['pileup_number_of_groups']) ? $settings['pileup_number_of_groups'] : '');
  $groups_labels = array();

  for ($group = 1; $group <= $groups; $group++) {
    $groups_labels['pileup-group-' . $group] = $settings['pileup_group_' . $group . '_label'];
  }
  return array('' => t('Select group')) + $groups_labels;
}

