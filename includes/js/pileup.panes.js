
(function($) {
  
  function destroyPileup() {
    $('.pileup-wrapper').each(function(){
      $(this).remove();
    });
    $('.pileup-processed').each(function() {
      $(this).removeClass('pileup-processed');
      $(this).addClass('pile-me-up');
      $(this).removeClass (function (index, css) {
        return (css.match (/([\w-]+pileup-here)$/) || []).join(' ');
      });
    });
  };
  
  Drupal.behaviors.pileup = {
    attach: function(context, settings) {

      $(window).on('resize load', function () {
        if($(window).width() < settings.pileup.breakpoint && !$('.panels-ipe-editing').length) {
          var groups = {};
          var groupClass = '';
          $('.pile-me-up').each(function() {
            if(!$(this).hasClass('pileup-processed')) {
                
              // Get group class
              groupClass = $.grep($(this).attr('class').split(" "), function(v, i) {
                return v.indexOf('pileup-group') === 0;
              });
        
              // @FIXME use clone.
              $content = $(this).parent().html().replace('pile-me-up','');
              
              if(groupClass in groups) {
                groups[groupClass] += $content
              } else {
                groups[groupClass] = $content;
                if(!$('.' + groupClass + '-wrapper').length) {
                  $(this).addClass(groupClass + '-pileup-here');
                }
              }   
              $(this).addClass('pileup-processed');
            }
          });
              
          // Wrap groups with container
          $.each(groups, function(groupClass, groupContent){
            if($('.' + groupClass + '-wrapper').length) {
              $('.' + groupClass + '-wrapper').append(groupContent);
            } else {
              groups[groupClass] = '<div class="' + groupClass + '-wrapper pileup-wrapper">' + groupContent + '</div>';
              $('.' + groupClass + '-pileup-here').after(groups[groupClass]);
            }
          });   
 
          $('.pileup-wrapper').each(function(){
            var owl = $(this)
            owl.owlCarousel(settings.pileup[groupClass])
          });
        }
        else {
          destroyPileup();
        }
      });
    }
  };

}(jQuery));
